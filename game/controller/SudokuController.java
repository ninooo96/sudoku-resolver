package controller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.plaf.ColorUIResource;

import model.Gioca;
import view.GraphicUI;
import view.Griglia;

public class SudokuController extends JPanel {// ex ViewObserver
	// vontroller per interagire con l'app
	private Gioca gioca;
	private int numSol, maxSol;
	private JButton risolvi, next, previous, reset;
	private JLabel label;
	private JTextField jtfSol;
//	private static JFrame frame = 
	
	public SudokuController(Griglia griglia) {
//		JFrame frame = gui.frame;
//		JPanel panel = gui.panel;

//		JPanel panelRight = new JPanel();
//		frame.add(panelRight);
//		JPanel buttonPanel = new JPanel();
//		System.out.println(frame.getHeight()/2);
//		add(buttonPanel);
//		buttonPanel.setBounds(frame.getHeight()/2, 0, frame.getWidth()-panel.getWidth(), frame.getHeight()/2);

//		buttonPanel.setPreferredSize(
//				new Dimension(GraphicUI.width - griglia.getWidth(), GraphicUI.height / 2));
//		panelRight.setLocation(frame.getHeight()/2, frame.getHeight());
		GridLayout gl2 = new GridLayout(10, 1);
//		buttonPanel.setBackground(Color.blue);
//		BoxLayout bl = new BoxLayout(target, axis);
//		FlowLayout fl = new FlowLayout();
//		fl.setAlignment(FlowLayout.TRAILING);

		risolvi = new JButton("Risolvi");
		reset = new JButton("Reset");
		next = new JButton("--->");
		previous = new JButton("<---");
		
		reset.setEnabled(false);
		next.setEnabled(false);
		previous.setEnabled(false);

		risolvi.addActionListener((ActionEvent e) -> {
			
			String input = leggi(griglia);
			try {
				if(jtfSol.getText()=="")
					maxSol=1;
				else
					maxSol = Integer.parseInt(jtfSol.getText());
				jtfSol.setText("");
				gioca = new Gioca(input, maxSol); 
				numSol = 1;
				gioca.resolver.stampaSoluzione(numSol);
				reset.setEnabled(true);
				risolvi.setEnabled(false);
				next.setEnabled(true);
				previous.setEnabled(true);
			}catch(NumberFormatException ex) {
				JOptionPane.showMessageDialog(null, "Devi inserire il numero massimo di soluzioni");
			}
//			if(numSol == 0 || numSol==)
//			scrivi(griglia, gioca.resolver.stampaSoluzione(numSol)); //gli passo una string e deve scrivere nella view questa stringa
			
		});
		
		reset.addActionListener((ActionEvent e)->{
			risolvi.setEnabled(true);
			next.setEnabled(false);
			previous.setEnabled(false);
			reset.setEnabled(false);
			StringBuilder sol = new StringBuilder();
//			for(int i = 0; i<81; i++) sol.append("0");
//			scrivi(griglia, sol.toString());
			gioca.resolver.stampaSoluzione(-1);
		});
		
		next.addActionListener((ActionEvent e) ->{
			try {

				previous.setEnabled(true);
				numSol+=1;
//				scrivi(griglia, gioca.resolver.stampaSoluzione(numSol));
				gioca.resolver.stampaSoluzione(numSol);
			}catch(Exception exc) {
				exc.printStackTrace();
				JOptionPane.showMessageDialog(null, "Non ci sono altre soluzioni");
				numSol-=1;
				next.setEnabled(false);
			}
		});
		
		previous.addActionListener((ActionEvent e)->{
			try {
				next.setEnabled(true);
				numSol-=1;
//				scrivi(griglia, gioca.resolver.stampaSoluzione(numSol));
				gioca.resolver.stampaSoluzione(numSol);
			}catch(Exception exc) {
				exc.printStackTrace();
				JOptionPane.showMessageDialog(null, "Sei alla prima soluzione");
				numSol+=1;
				previous.setEnabled(false);
			}
		});
		label = new JLabel("numero max di soluzioni");
		jtfSol = new JTextField();
		
		setLayout(gl2);
		add(label);
		add(risolvi);
		add(reset);
		add(next);
		add(previous);
		add(label);
		add(jtfSol);

	}
	
	private void scrivi(Griglia gui, String sol) {
		char[] tmp = sol.toCharArray();
		ArrayList<JTextField[]> regioni = gui.regioni;
		int cont = 0;
		int i = 0;
		int j = 0;
		int riga = 0;
		boolean flag = true;
		while (flag) {
			for (int x = 0; x < 3; x++) {
				System.out.println("i-> " + i + ",j-> " + j);
				char temp = tmp[cont];//regioni.get(i)[j].getText();
				if (temp == '0')
					regioni.get(i)[j].setText("");
				else
					regioni.get(i)[j].setText(temp+"");
				cont++;
				j++;
			}
			i++;
			j -= 3;
			if (i % 3 == 0) {
				riga++;
				i -= 3;
				j += 3;
			}
			if (riga == 3) {
				riga = 0;
				i += 3;
				j = 0;
			}
			if (i==9)
				flag = false;
//			if(i==3 ) { i=0; j+=3;}
		}
//		StringBuilder sb = new StringBuilder();
//		for(i=0;i<tmp.length; i++)
//			sb.append(tmp[i]);
//		return sb.toString();
	
	}

	private String leggi(Griglia gui) {
		char[] tmp = new char[81];
		ArrayList<JTextField[]> regioni = gui.regioni;
		int cont = 0;
		int i = 0;
		int j = 0;
		int riga = 0;
		boolean flag = true;
		while (flag) {
			for (int x = 0; x < 3; x++) {
				System.out.println("i-> " + i + ",j-> " + j);
				String temp = regioni.get(i)[j].getText();
				if (temp.trim().equals(""))
					tmp[cont] = '0';
				else if(temp.trim().matches("[1-9]") )
					tmp[cont] = temp.charAt(0);
				else {
					JOptionPane.showMessageDialog(null, "Controlla il formato");
					risolvi.setEnabled(true);
					next.setEnabled(false);
					previous.setEnabled(false);
					break;
				}
				cont++;
				j++;
			}
			i++;
			j -= 3;
			if (i % 3 == 0) {
				riga++;
				i -= 3;
				j += 3;
			}
			if (riga == 3) {
				riga = 0;
				i += 3;
				j = 0;
			}
			if (i==9)
				flag = false;
//			if(i==3 ) { i=0; j+=3;}
		}
		StringBuilder sb = new StringBuilder();
		for(i=0;i<tmp.length; i++)
			sb.append(tmp[i]);
		return sb.toString();
	}
	}
