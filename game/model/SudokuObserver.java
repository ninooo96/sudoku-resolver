package model;

public interface SudokuObserver {
	public void update(String sol);
}
