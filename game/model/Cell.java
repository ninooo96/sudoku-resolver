package model;

import java.util.ArrayList;
import java.util.List;


public class Cell {
	private int i,j;
	private int value;
	private boolean modificable = true;
	private boolean visited = false;
	
	public Cell(int i, int j) {
		this.i = i;
		this.j = j;	
	}
	
	public boolean isModificable() {
		return modificable;
	}
	
//	public void setVisited(boolean bool) {
//		visited = bool;
//	}
//	
//	public boolean isVisited() {
//		return visited;
//	}
	
	public void setModificable(boolean bool) {
		modificable = bool;
	}
	
		
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	
	public int getI() {
		return i;
	}
	
	public int getJ() {
		return j;
	}
	
	@Override
	public boolean equals(Object o) {
		if(this == o) return true;
		if(!( o instanceof Cell)) return false;
		Cell c = (Cell) o;
		if(this.getI() == c.getI() && this.getJ() == c.getJ()) return true;
		return false;
	}
	
	public static void main(String... args) {
		for( char c: "ciaoo".toCharArray()) {
			System.out.println(c);
		}
	}
	
}
