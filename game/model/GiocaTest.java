package model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GiocaTest {
	private Gioca gioca;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGioca() {
		try {
		gioca = new Gioca("0394", 1);
		fail("L'input di configurazione deve avere 81 caratteri");
		gioca = new Gioca("010020300000003040050000006004700050000100003070068000300004090000600104006000000", -1);
		fail("Il numero massimo di soluzioni deve essere >0");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
