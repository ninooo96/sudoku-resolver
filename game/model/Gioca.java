package model;

import java.util.ArrayList;
import java.util.Scanner;

import model.Resolver.SoluzioneMemento;


public class Gioca {
	public Cell[][] tabella;
	public Resolver resolver;
	public static ArrayList<Resolver.SoluzioneMemento> mementoList;
	
	public Gioca(String init, int maxSol) {
		if(init.length()!=81) throw new IllegalStateException("Lunghezza input diversa da 81");
		if(maxSol<=0) throw new IllegalStateException("Numero massimo soluzioni on positivo");
		tabella = new Cell[9][9];
		for(int i = 0; i < 9; i++) {
			for(int j = 0; j< 9; j++) {
				tabella[i][j]= new Cell(i+1, j+1) ;
			}
		}
		mementoList = new ArrayList<>();
		popola(init);
		resolver = new Resolver(tabella);
		resolver.risolvi(maxSol);
	}
	
	private void popola(String init) {
		int cont = 0;
		char[] array = init.toCharArray();
		for(int i = 0; i < 9; i++) {
			for(int j = 0; j< 9; j++) {
				tabella[i][j].setValue(Integer.parseInt(array[cont]+""));
				if(Integer.parseInt(array[cont]+"") != 0)
					tabella[i][j].setModificable(false);
				cont++;				
			}
		}
	}
	
	public static void main (String[] args) {
		
			Gioca gioca = new Gioca("010020300000003040050000006004700050000100003070068000300004090000600104006000000", 15);
			
			for(int i = 0; i < 9; i++) {
				for(int j = 0; j< 9; j++) {
					System.out.print(gioca.tabella[i][j].getValue()+" ");
					if((j+1)%3==0) System.out.print("|");
				}
				System.out.println();
				if((i+1)%3==0) System.out.println("---------------------");
			}
			System.out.println();
			
		Scanner sc = new Scanner(System.in);
	int tmp = 0;
	while(true) {
		tmp = sc.nextInt();
//		resolver.stampaSoluzione(tmp);
	}
	
	}
	
	
	

}
