package model;

import java.util.ArrayList;

import backtracking.Problema;
import view.GraphicUI;
import view.Griglia;

public class Resolver implements Problema <Cell, Integer> {
	public Cell[][] tabella;
	public Resolver originatorState;
//	public ArrayList<SoluzioneMemento> mementoList;
	public ArrayList<SudokuObserver> observers = new ArrayList<>();
	//	private Iterator<SoluzioneMemento> it;

	public Resolver(Cell[][] tabella) {
		this.tabella = tabella;
		addObserver(GraphicUI.griglia);
//		mementoList = new ArrayList<>();
//		it = mementoList.iterator();
	}

	@Override
	public Cell primoPuntoDiScelta() {
		if(tabella[0][0].isModificable()) {
			return tabella[0][0];
		}
		return prossimoPuntoDiScelta(tabella[0][0]);
	}

	@Override
	public Cell prossimoPuntoDiScelta(Cell ps) {
		if (ps.equals(ultimoPuntoDiScelta()))
			return ultimoPuntoDiScelta();// soluzione trovata
		else if (ps.getJ() == 9) {
			if(tabella[ps.getI()][0].isModificable())
				return tabella[ps.getI()][0];// la cella parte dall'indice 1, non da 0
			else return prossimoPuntoDiScelta(tabella[ps.getI()][0]);
		}
		else {
			if (tabella[ps.getI() - 1][ps.getJ()].isModificable())
				return tabella[ps.getI() - 1][ps.getJ()];
			else return prossimoPuntoDiScelta(tabella[ps.getI() - 1][ps.getJ()]);
		}
	}

	@Override
	public Cell ultimoPuntoDiScelta() {
		if(tabella[8][8].isModificable())
			return tabella[8][8];
		return precedentePuntoDiScelta(tabella[8][8]);
	}

	@Override
	public Integer primaScelta(Cell ps) {
		return 1;//ps.getValoriLegali().get(0);
	}

	@Override
	public Integer prossimaScelta(Integer s) {
		return s+1; // il controllo lo fa il metodo risolvi
	}

	@Override
	public Integer ultimaScelta(Cell ps) {
		return 9;//ps.getValoriLegali().get(8);
	}

	@Override
	public boolean assegnabile(Integer scelta, Cell puntoDiScelta) {
		if(!puntoDiScelta.isModificable()) return false;
		int indColonna = puntoDiScelta.getJ()-1;
		int indRiga = puntoDiScelta.getI()-1;
		//System.out.println(indRiga+"   "+indColonna);
		for (int i = 0; i<9; i++) {
			//controllo colonna
			if(tabella[i][indColonna].getValue() == scelta) return false;
			//controllo riga
			if(tabella[indRiga][i].getValue() == scelta) return false;
		}
		//controllo quadratino
		return controllaRegione(indRiga, indColonna, scelta);
	}

	private boolean controllaRegione(int indRiga, int indColonna, Integer scelta) {
		if(indRiga<3) {
			if(indColonna<3) {
				for(int i = 0; i<3;i++) {
					for( int j = 0; j<3;j++) {
						if(tabella[i][j].getValue() == scelta) return false;
					}
				}
			}
			else if(indColonna < 6) {
				for(int i = 0; i <3;i++) {
					for( int j = 3; j<6;j++) {
						if(tabella[i][j].getValue() == scelta) return false;
					}
				}
			}
			else{
				for(int i = 0; i <3;i++) {
					for( int j = 6; j<9;j++) {
						if(tabella[i][j].getValue() == scelta) return false;
					}
				}
			}
		}
		
		else if(indRiga<6) {
			if(indColonna<3) {
				for(int i = 3; i <6;i++) {
					for( int j = 0; j<3;j++) {
						if(tabella[i][j].getValue() == scelta) return false;
					}
				}
			}
			else if(indColonna < 6) {
				for(int i = 3; i <6;i++) {
					for( int j = 3; j<6;j++) {
						if(tabella[i][j].getValue() == scelta) return false;
					}
				}
			}
			else {
				for(int i = 3; i <6;i++) {
					for( int j = 6; j<9;j++) {
						if(tabella[i][j].getValue() == scelta) return false;
					}
				}
			}
		}
		
		else if(indRiga<9) {
			if(indColonna<3) {
				for(int i = 6; i <9;i++) {
					for( int j = 0; j<3;j++) {
						if(tabella[i][j].getValue() == scelta) return false;
					}
				}
			}
			else if(indColonna < 6) {
				for(int i = 6; i <9;i++) {
					for( int j = 3; j<6;j++) {
						if(tabella[i][j].getValue() == scelta) return false;
					}
				}
			}
			else {
				for(int i = 6; i <9;i++) {
					for( int j = 6; j<9;j++) {
						if(tabella[i][j].getValue() == scelta) return false;
					}
				}
			}
		}
		return true;
	}

	@Override
	public void assegna(Integer scelta, Cell puntoDiScelta) {
		if (assegnabile(scelta, puntoDiScelta)) {
			tabella[puntoDiScelta.getI()-1][puntoDiScelta.getJ()-1].setValue(scelta);
//			tabella[puntoDiScelta.getI()-1][puntoDiScelta.getJ()-1].setVisited(true);
		}
	}

	@Override
	public void deassegna(Integer scelta, Cell puntoDiScelta) {
		tabella[puntoDiScelta.getI()-1][puntoDiScelta.getJ()-1].setValue(0);
//		tabella[puntoDiScelta.getI()-1][puntoDiScelta.getJ()-1].setVisited(false);
		//non sono sicuroo
	}

	@Override
	public Cell precedentePuntoDiScelta(Cell puntoDiScelta) {
		if(puntoDiScelta.equals(primoPuntoDiScelta())) return primoPuntoDiScelta();
		//il caso in cui viene invocato nel primo punto di scelta è gestito da Problema
		else if(puntoDiScelta.getJ()==1 ) {
			if(tabella[puntoDiScelta.getI()-2][8].isModificable())
				return tabella[puntoDiScelta.getI()-2][8];
			return precedentePuntoDiScelta(tabella[puntoDiScelta.getI()-2][8]);
		}
		else {
			if(tabella[puntoDiScelta.getI()-1][puntoDiScelta.getJ()-2].isModificable())
				return tabella[puntoDiScelta.getI()-1][puntoDiScelta.getJ()-2];
			return precedentePuntoDiScelta(tabella[puntoDiScelta.getI()-1][puntoDiScelta.getJ()-2]);
		}
		
	}

	@Override
	public Integer ultimaSceltaAssegnataA(Cell puntoDiScelta) {
		return puntoDiScelta.getValue();
	}
	
	@Override
	public void scriviSoluzione(int nr_sol) {
		SoluzioneMemento memento = getMemento();
		Gioca.mementoList.add(memento);
//		for(int i = 0; i < 9; i++) {
//			for(int j = 0; j< 9; j++) {
//				System.out.print(tabella[i][j].getValue()+" ");
//				if((j+1)%3==0) System.out.print("|");
//			}
//			System.out.println();
//			if((i+1)%3==0) System.out.println("---------------------");
//		}
//		System.out.println();
	}
	
	public String stampaSoluzione(int x) {
		if(x<0) {
			String reset = new String(new char[81]).replace("\0", "0");
			notify(reset);
			return reset;
		}
//		if(x== 0) {
//			System.out.println("Parti da 1!!!");
//			return null;
//		}
//		if(x>mementoList.size()) {
//			System.out.println("Ho trovato solo "+ mementoList.size()+" soluzioni");
//			return null;
//		}
		SoluzioneMemento memento = Gioca.mementoList.get(x-1);
		setMemento(memento);
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < 9; i++) {
			for(int j = 0; j< 9; j++) {
				sb.append(tabella[i][j].getValue());
				System.out.print(tabella[i][j].getValue()+" ");
				if((j+1)%3==0) System.out.print("|");
			}
			System.out.println();
			if((i+1)%3==0) System.out.println("---------------------");
		}
		System.out.println();
		notify( sb.toString());//Griglia è l'unico Observer di Resolver
		return sb.toString();
	}
	
	public void addObserver(SudokuObserver obs) {
		if(!observers.contains(obs))
			observers.add(obs);
	}
	
	public void removeObserver(SudokuObserver obs) {
		if(observers.contains(obs))
			observers.remove(obs);
	}
	
	private void notify(String sol) {
		for(SudokuObserver sObs: observers)
			sObs.update(sol);
	}
	
	public SoluzioneMemento getMemento() {
		return new SoluzioneMemento(tabella);
	}
	
	public void setMemento(SoluzioneMemento memento) {
		for(int i = 0; i<9; i++) {
			for(int j = 0; j<9; j++) {
				this.tabella[i][j].setValue(memento.mementoState[i][j].getValue()); 
			}
		}
	}
	
	
	
	public class SoluzioneMemento{
		private Cell[][] mementoState;
		
		public SoluzioneMemento(Cell[][] tabella) {
			mementoState = new Cell[9][9];
			for(int i = 0; i<9; i++) {
				for(int j = 0; j<9; j++) {
					Cell tmp = new Cell(i,j);
					tmp.setValue(tabella[i][j].getValue());
					mementoState[i][j] = tmp; 
				}
			}
		}
		
		 
    }

}
