package view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import model.SudokuObserver;

public class Griglia extends JPanel implements SudokuObserver {
	public int widthScreen, heightScreen;
	public JFrame frame;
	public ArrayList<JTextField[]> regioni = new ArrayList<>();
	public JPanel panel;
	public String sol="";

	public Griglia() {
		init();
//		System.out.println(this.getComponentCount());
	}
	
	public String getSol() {
		return sol;
	}
	
	public void update(String sol) {
		if(sol.length()!=81) throw new IllegalStateException();
		this.sol = sol;
		char[] tmp = sol.toCharArray();
//		ArrayList<JTextField[]> regioni = gui.regioni;
		int cont = 0;
		int i = 0;
		int j = 0;
		int riga = 0;
		boolean flag = true;
		while (flag) {
			for (int x = 0; x < 3; x++) {
				System.out.println("i-> " + i + ",j-> " + j);
				char temp = tmp[cont];//regioni.get(i)[j].getText();
				if (temp == '0')
					regioni.get(i)[j].setText("");
				else
					regioni.get(i)[j].setText(temp+"");
				cont++;
				j++;
			}
			i++;
			j -= 3;
			if (i % 3 == 0) {
				riga++;
				i -= 3;
				j += 3;
			}
			if (riga == 3) {
				riga = 0;
				i += 3;
				j = 0;
			}
			if (i==9)
				flag = false;
//			if(i==3 ) { i=0; j+=3;}
		}
	}

	private void init() {
//		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//		widthScreen = (int) screenSize.getWidth();
//		heightScreen = (int) screenSize.getHeight();
//		frame = new JFrame("Sudoku");
//		frame.setLayout(new BorderLayout());
//
//		frame.setBounds(widthScreen / 2 - (int) (widthScreen / 3.6), heightScreen / 2 - (heightScreen / 3),
//				(int) (widthScreen / 1.8), (int) (heightScreen / 1.5));//trovarlo meglio su internet(come centrare un frame)
//		panel = new JPanel();
//		frame.setMinimumSize(new Dimension(widthScreen/2, heightScreen/2));
//		frame.setResizable(false);
		
		setPreferredSize(new Dimension(GraphicUI.height, GraphicUI.height));

		//		frame.addWindowStateListener(e-> {
//			if (e.getWindow().getMaximumSize().getWidth() == frame.MAXIMIZED_BOTH)
//				panel.setPreferredSize(new Dimension(heightScreen,heightScreen));
//		
//		});
		GridLayout lm = new GridLayout(3, 3);
		setLayout(lm);
		setBounds(0, 0, heightScreen / 2, heightScreen / 2);
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				add(drawRegione());
//			JTextField jtf = new JTextField();
//			jtf.setSize(panel.getHeight()/9, panel.getHeight()/9);
//			Font f = new Font(name, style, size);
//			jtf.setFont(f);
//			panel.add(jtf);
//			if(i%3==0)
//				panel.add(new JLabel("|"));

			}
			lm.setHgap(8);
			lm.setVgap(8);
		}
		
		
//		JPanel panelRight = new JPanel();
//		frame.add(panelRight);
//		JPanel buttonPanel = new JPanel();
////		System.out.println(frame.getHeight()/2);
//		panelRight.add(buttonPanel);
////		buttonPanel.setBounds(frame.getHeight()/2, 0, frame.getWidth()-panel.getWidth(), frame.getHeight()/2);
//		
//		
//		buttonPanel.setPreferredSize(new Dimension(frame.getWidth()-panel.getWidth(), frame.getHeight()/2));
////		panelRight.setLocation(frame.getHeight()/2, frame.getHeight());
//		GridLayout gl2 = new GridLayout(4, 1);
////		buttonPanel.setBackground(Color.blue);
////		BoxLayout bl = new BoxLayout(target, axis);
////		FlowLayout fl = new FlowLayout();
////		fl.setAlignment(FlowLayout.TRAILING);
//		buttonPanel.setLayout(gl2);
//		buttonPanel.add(new JButton("Risolvi"));
//		buttonPanel.add(new JButton("Reset"));
//		buttonPanel.add(new JButton("--->"));
//		buttonPanel.add(new JButton("<---"));
	}

//	private class Pannello extends JPanel{ //esempio di JComponent
//	      public void paintComponent(Graphics g){
//	    	 super.paintComponent(g);
//	    	 this.setBounds(0, 0, frame.getHeight(), frame.getHeight());
////	    	 this.setPreferredSize(new Dimension(frame.getHeight(), frame.getHeight()));
////	    	 if(frame.getExtendedState() == JFrame.MAXIMIZED_HORIZ)
////		    	 this.setPreferredSize(new Dimension(frame.getHeight(), frame.getHeight()));
//
//	      }
//	}

	public JPanel drawRegione() {
		JPanel panelTmp = new JPanel();
		GridLayout gLayout = new GridLayout(3, 3);
		JTextField[] tmp = new JTextField[9];
		panelTmp.setLayout(gLayout);
		for (int i = 0; i < 9; i++) {
			JTextField jtf = new JTextField();
			jtf.setHorizontalAlignment((int) CENTER_ALIGNMENT);
			jtf.setSize(getHeight() / 9, getHeight() / 9);
			tmp[i] = jtf;
			panelTmp.add(jtf);
		}
		regioni.add(tmp);
		return panelTmp;
	}

	public static void main(String[] args) {
		new Griglia();
	}

}
