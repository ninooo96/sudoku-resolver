package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;

import controller.SudokuController;

public class GraphicUI extends JFrame{
	private static int widthScreen;
	private static int heightScreen;
//	public static JFrame frame;
	public static Griglia griglia;
	public static int width, height;

	public GraphicUI() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		widthScreen = (int) screenSize.getWidth();
		heightScreen = (int) screenSize.getHeight();
//		frame = new JFrame("Sudoku");
		 setTitle("Sudoku");
		setLayout(new BorderLayout());
		
		
		
		setSize(new Dimension((int) (widthScreen / 2), (int) (heightScreen / 1.5)));
		width = getWidth();
		height = getHeight();
		setLocation(new Point((screenSize.width - getSize().width) / 2, 
		(screenSize.height - getSize().height) / 2 ));

//		setBounds(widthScreen / 2 - (int) (widthScreen / 3.6), heightScreen / 2 - (heightScreen / 3),
//				(int) (widthScreen / 1.8), (int) (heightScreen / 1.5));//trovarlo meglio su internet(come centrare un frame)
		griglia = new Griglia();
		add(griglia, BorderLayout.WEST);
		add(new SudokuController(griglia));
		setVisible(true);
		
		
	}
	
	public static void main(String... args) {
		new GraphicUI();
	}
}
