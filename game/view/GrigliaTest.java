package view;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class GrigliaTest{

	private Griglia griglia;

	@Before
	public void setUp() throws Exception {
		System.out.println("entra");
		griglia = new Griglia();
	}

	@After
	public void tearDown() throws Exception {
		griglia = null;
	}

	@Test
	public void testGriglia() {
		assertEquals("La griglia non è composta da 9 regioni", 9, griglia.getComponentCount());
	}

	@Test
	public void testUpdateString() {
//			griglia = new Griglia();
		try {
			griglia.update("0000");
			fail("La stringa che rappresenta una configurazione non può avere meno di 81 caratteri");
		} catch (IllegalStateException e) {
//				if(griglia != null) System.out.println("ciao");
//			assertEquals("La stringa non ha il numero corretto di caratteri", 81, griglia.getSol().length());
		}
	}
	
	@Test
	public void testDrawRegione() {
		assertEquals("Il numero di elementi appartenenti ad una regione non sono pari a 9", 9, griglia.drawRegione().getComponentCount());
	}

}
